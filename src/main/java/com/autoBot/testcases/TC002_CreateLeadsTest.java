package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC002_CreateLeadsTest extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLeadsTest";
		testcaseDec = "Check the Create Leads";
		author = "Krishna";
		category = "smoke";
		excelFileName = "TC002";
	}

	@Test(dataProvider="fetchData")
	public void CreateLeads(String uName, String pwd, String cName, String fName, String lName)
	{	
		new LoginPage()

		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFALink()
		.clickLeadsMenu()
		.clickCreateLeads()
		.enterCompanyName(cName)
		.enterFirstName(fName)
		.enterLastName(lName)
		.clickCreateLeadbutton()
		.verifyFirstName(fName);

	}


}
