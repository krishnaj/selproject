package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadsPage extends Annotations{

	public CreateLeadsPage enterCompanyName(String data) {

		WebElement eleCompanyName = locateElement("Id","createLeadForm_companyName");
		clearAndType(eleCompanyName, data);
		//	driver.findElementById("createLeadForm_companyName").sendKeys(data);
		return this;
	}

	public CreateLeadsPage enterFirstName(String data) {
		WebElement eleFirstName = locateElement("Id","createLeadForm_firstName");
		clearAndType(eleFirstName, data);
		//	driver.findElementById("createLeadForm_firstName").sendKeys(data);
		return this;
	}

	public CreateLeadsPage enterLastName(String data) {
		WebElement eleLastName = locateElement("Id","createLeadForm_lastName");
		clearAndType(eleLastName, data);
		//driver.findElementById("createLeadForm_lastName").sendKeys(data);
		return this;
	}

	public ViewLeadsPage clickCreateLeadbutton() {

		WebElement eleCreateLeadButton = locateElement("Class","smallSubmit");
		click(eleCreateLeadButton);
		//	driver.findElementByClassName("smallSubmit").click();
		return new ViewLeadsPage();
	}

}
