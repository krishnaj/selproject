package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class ViewLeadsPage extends Annotations{
	
	public ViewLeadsPage verifyFirstName(String data) {
		
		WebElement eleVerifyFirstName = locateElement("Id","viewLead_firstName_sp");
		String valueFirstName = eleVerifyFirstName.getText();
//		String firstName = driver.findElementById("viewLead_firstName_sp").getText();
		if (valueFirstName.equals(data))
		{
			System.out.println("Entered First Name is Matched");
		}
		else
		{
			System.out.println("Enter First Name is MisMatched");
		}
		return this;
	}

}
