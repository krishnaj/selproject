package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import com.autoBot.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations{

	public CreateLeadsPage clickCreateLeads()
	{
		WebElement eleCreateLead = locateElement("Link", "Create Lead");
		click(eleCreateLead);
//		driver.findElementByLinkText("Create Lead").click();
		return new CreateLeadsPage();
	}
}
