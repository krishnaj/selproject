package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations{

	public MyLeadsPage clickLeadsMenu()
	{
		WebElement eleLeadMenu = locateElement("Link", "Leads");
		click(eleLeadMenu);
//		driver.findElementByLinkText("Leads").click();
		return new MyLeadsPage();
	}
}
